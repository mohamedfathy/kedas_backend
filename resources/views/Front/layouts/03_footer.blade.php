    <!-- start section -->
    <footer class="footer bg-cover" style=" background-image: url('/assets/images/footer-background.jpg');">
      <div class="footer-overlay">
        <div class="container footer-content">
          <div class="row justify-content-between">
            <div class="col-lg-9 order-lg-1 order-2">
              <div class="row justify-content-between">
                <div class="col-md-6">
                  <div class="links">
                    <h5>{{ __('main.footer.contact_us')}}</h5>
                    <ul class="list-unstyled footer-links">  

                      <li><a href="#"><i class="fas fa-phone mx-1"></i> <span style="  direction: ltr;display: inline-block;">@foreach($phones as $phone)  {{$phone->phone}} @endforeach</span></a></li>
                      <li><a href="#"><i class="fas fa-envelope mx-1"></i> @foreach($emails as $email) {{$email->email}} @endforeach</a></li>
                      <li><a href="#"><i class="fas fa-map-marker-alt mx-1"></i> {{ __('main.footer.first_address')}}</a></li>
                      <li><a href="#"><i class="fas fa-map-marker-alt mx-1"></i> {{ __('main.footer.second_address')}}</a></li>
                    </ul>
                  </div>
                </div>

                <div class="col-md-5">
                  <div class="links">
                    <h5>{{ __('main.footer.browse')}}</h5>
                    <ul class="list-unstyled">
                      <li><a href="{{ route('home', app()->getLocale() )}}">{{ __('main.home')}}</a></li>
                      <li><a href="{{ route('categories', app()->getLocale() )}}">{{ __('main.products')}}</a></li>
                      <li><a href="{{ route('news', app()->getLocale() )}}">{{ __('main.news')}}</a></li>
                      <li><a href="{{ route('about', app()->getLocale() )}}">{{ __('main.about')}}</a></li>
                      <li><a href="{{ route('contact', app()->getLocale() )}}">{{ __('main.contact')}}</a></li>
                    </ul>
                  </div>
                </div>

              </div>
            </div>
            <div class="col-lg-3 order-lg-2 order-1">
              <p class="text-dark footer-heading-text">
                <a href="/{{app()->getLocale()}}">{{ __('main.title')}}</a>
              </p>
              <ul class="list-unstyled d-flex align-items-center social-icons">
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                <li><a href="#"><i class="fab fa-behance"></i></a></li>
              </ul>
            </div>
          </div>
        </div>

        <div style="background-color: rgba(0, 0, 0, 0.1); padding: 20px 0;">
          <p class="text-center text-dark mb-0" > {{ __('main.footer.start_copyright') }} © <?php echo date('Y'); ?> {{ __('main.footer.company_copyright') }}, {{ __('main.footer.end_copyright') }}.</p>
        </div>
      </div>
    </footer>
    <!-- end section -->
    
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/assets/js/vendor/jquery_v3.4.1.js"></script>
    <script src="/assets/js/vendor/popper_v1.6.0.js"></script>
    <script src="/assets/js/vendor/bootstrap_v4.4.1.js"></script>
    <script src="/assets/js/main.js"></script>

    <!-- datatable plugin -->
    <!-- <script src="/assets/plugins/datatable/js/jquery_v3.3.1.js"></script>  -->
    <script src="/assets/plugins/datatable/js/jquery.dataTables_v1.10.20.min.js"></script> 
    <script src="/assets/plugins/datatable/js/dataTables.bootstrap4_v1.10.20.min.js"></script> 
    <script>
      $(document).ready(function() {
          $('#productsTable').DataTable();
      });
    </script>
  </body>
</html>