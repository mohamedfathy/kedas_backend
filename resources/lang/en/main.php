<?php
return [
    // general files
    'title' => 'Kedas',
    'home' => 'Home',
    'categories' => 'Categories',
    'products' => 'Products',
    'product' => 'Product',
    'news' => 'News',
    'about' => 'About',
    'contact' => 'Contact',

    // Front/layouts/03_footer.blade.php
    'footer.contact_us' => 'Contact us',
    'footer.first_address' => '78, Adnan Almalki st - El-Minya - Egypt',
    'footer.second_address' => 'Elahad Elgeded st - Samalot - El-Minya - Egypt',
    'footer.browse' => 'Browse',
    'footer.start_copyright' => 'Copyright',
    'footer.company_copyright' => 'Kedas Company',
    'footer.end_copyright' => 'All rights reserved.',

    // Front/partials/contact.blade.php
    'contact.lets_talk' => 'LET\'S TALK',
    'contact.name' => 'Name',
    'contact.name_validation' => 'Please provide a name.',
    'contact.phone' => 'Phone',
    'contact.phone_validation' => 'Please provide a phone.',
    'contact.message' => 'Message',
    'contact.message_validation' => 'Please provide a message.',
    'contact.send' => 'Send',

    // Front/home.blade.php
    'home.company_slogan' => 'THE EXPERTS FOR HIGH QUALITY CUTTING AND GRINDING DISCS',
    'home.company_title' => 'El Masria Company',
    'home.exclusive_partner' => 'Exclusive Partner in Egypt',
    'home.owner_name' => 'En/ Ahmed Abdalla',
    'home.company_owner' => 'El Masria Company Owner',
    'home.company_story' => 'The company was founded on 1999 and is working in the field of distribution of Building and construction, tools and machinery.',
    'home.read_more' => 'Read More',

    // Front/about.blade.php
    'about.our_story' => 'Our Story',
    'about.company_story' => 'The company was founded on 1999 and is working in the field of distribution of Building and construction, tools and machinery.
    The company is directed by its owner Mr. Eng / Ahmed Abdullah Mohammed Abdullah.
    El Masria is a sole agent/distributor for major building materials companies inside and outside of the Arab Republic of Egypt.
    In 2012, a contract was signed between KEDAS GROUP -supplier of cutting and grinding discs and diamond blades - from Germany and El Masria to be the sole and exclusive distributor of KEDAS GROUP products in Egypt.',

    // Front/categories.blade.php
    'categories.categories' => 'Categories',
    'categories.our_categories' => 'Our Categories',
    'categories.explore' => 'Explore',

    // Front/news.blade.php
    'news.hardware_exhibition' => 'Hardware Exhibition 2012',
    'news.exhibtion_description' => 'Egyption partner and KEDAS GROUP team at the International Hardware Exhibition Cologne in 2012.',

    // Front/product.blade.php
    'product.characteristics' => 'Characteristics',
    'product.application' => 'Applications',
    'product.components' => 'Components',
    'product.type' => 'Type',
    'product.edge' => 'Edge',
    'product.quality' => 'Quality',
    'product.safety_signs' => 'Safety Signs',
    'product.dim' => 'Dim',
    'product.shape' => 'SHAPE',
    'product.max_speed' => 'MAX SPEED',
    'product.max_rpm' => 'MAX RPM',
    'product.segement_no' => 'SEGEMENT NO',
    'product.segement_thickness' => 'SEGMENT THICKNESS',
    'product.segement_height' => 'SEGMENT HEIGHT',
    'product.segement_quality' => 'QUALITY',
    'product.product_data' => 'Product Data',
    'product.service_material' => 'Service Material',
];