<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $table = 'products';
  public $timestamps = false;

  public function category () 
  {
    return $this->belongsTo('App\Models\Category');
  }

  public function signs()
  {
    return $this->belongsToMany('App\Models\Sign','product_signs');
  }

  public function sizes()
  {
    return $this->hasMany('App\Models\ProductSize', 'product_id');
  }
}
