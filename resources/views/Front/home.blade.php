@extends('Front.layouts.02_master')
@section('title', __('main.home'))
@section('content')
<!--start section-->
<section class="main-section bg-cover skewed index-main-section" style="background-image: url('/assets/images/background.jpg');">
  <div class="overlay">
    <div class="container unskewed">
      <div class="row">
        <div class="col-12">
          <h1 class="text-center nunito-heading different">{{ __('main.home.company_slogan') }}</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!--end section-->

<!-- start main -->
<main>
  <div class="container text-dark lead" >
    <div class="row">
      <div class="col-md-12 our-partner-box text-center">
        <h1 class="display-1">{{ __('main.home.company_title') }}</h1> 
        <p class="display-4"> {{ __('main.home.exclusive_partner') }}</p>
        <img src="/assets/images/owner.jpg" width="150px" height="150px" alt="Owner of El Masria Company" class="rounded-circle" style="border:8px solid #ffffff;">
        <p class="lead text-center"> {{ __('main.home.owner_name') }}</p>
        <p class="lead text-center"> {{ __('main.home.company_owner') }}</p>
        <p class="text-truncate">{{ __('main.home.company_story') }}</p>
        <div class="form-group text-center">
          <a href="/{{ app()->getLocale() }}/about" class="btn btn-dark px-5">{{ __('main.home.read_more') }}</a>
          <!-- <button type="submit" class="btn btn-dark main-btn black-btn px-5">Read More</button> -->
        </div>
      </div>
    </div>
  </div>
</main>
<!-- end main -->

@include('Front.partials.contact')
@include('Front.partials.map')
@endsection