<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\Complaint;
use App\Models\Sign;
use App\Models\CategoryProduct;
use App\Models\DataSheet;
use App\Models\ServiceMaterial;
use Validator;
use App;

class FrontController extends Controller
{
  public function home () 
  {
    return view('Front.home');
  }

  public function categories () 
  {
    $categories = Category::all();
    return view('Front.categories', compact('categories'));
  }

  public function products (Request $request) 
  {
    $category = Category::find($request->category_id);
    $productsbyCatgory = CategoryProduct::where('category_id', $request->category_id)->pluck('product_id');
    $products = Product::find($productsbyCatgory);
    return view('Front.products', compact('category', 'products'));
  }

  public function product (Request $request)
  {
    $product = Product::find($request->product_id);
    $data_sheet = DataSheet::where('product_id', $request->product_id)->first();
    $service_material = ServiceMaterial::all();
    return view('Front.product', compact('product', 'data_sheet', 'service_material'));
  }

  public function news () 
  {
    return view('Front.news');
  }

  public function about () 
  {
    return view('Front.about');
  }

  public function contact () 
  {
    return view('Front.contact');
  }

  public function complaints (Request $request)
  {
    $validatedData = $request->validate([
      'name' => 'required|min:3|max:100',
      'phone' => 'required|regex:/^\+?\d[0-9-]{6,15}$/',
      'message' => 'required|min:3'
    ]);

    $Complaint = new Complaint();
    $Complaint->name = $request->name;
    $Complaint->phone = $request->phone;
    $Complaint->message = $request->message;
    $Complaint->save();
    
    // get the current language.
    $locale = App::getLocale();
    return redirect("/{$locale}");
  }
}
