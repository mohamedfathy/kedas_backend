@extends('Front.layouts.02_master')
@section('title',  __('main.about'))
@section('content')
<!--start section-->
<section class="main-section bg-cover skewed" style="background-image: url('/assets/images/background.jpg');">
  <div class="overlay">
    <div class="container unskewed">
      <div class="row">
        <div class="col-12">
          <h1 class="text-center nunito-heading"> {{ __('main.about')}}</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!--end section-->

<!-- start main -->
<main class="text-dark">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 our-partner-box text-center offset-lg-2">
          <h1 class="display-4 mb-4">{{ __('main.about.our_story')}}</h1>
          <p class="lead text-center">{{ __('main.about.company_story')}}</p>
      </div>
    </div>
</main>
<!-- end main -->

@include('Front.partials.contact')
@include('Front.partials.map')
@endsection