@extends('Front.layouts.02_master')
@section('title', __('main.product'))
@section('content')
<!--start section-->
<section class="main-section bg-cover" style="min-height: 50vh; background-image: url('/assets/images/background.jpg');">
  <div style="min-height: 50vh;  background-color: rgba(0, 0, 0, 0.7);padding: 80px 0 60px;">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h1 class="text-center nunito-heading text-light">{{$product->name}}</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!--end section-->

<!-- start main--> 
<main class="conpag-main text-dark" style="background-color: #ffffff;">
  <div class="container">
    <div class="row pt-5">
      <div class="col-lg-8 text-center">
        <img src="{{$product->img_url}}" alt="{{$product->name}}" class="img-fluid" style="background-color: #f2f2f2;">
      </div>
      <div class="col-lg-4 mt-5 pl-5" >
        <h1 class="mb-4">{{$product->name}}</h1>

        @if($product->characteristics !== null)
        <h3 class="lead" > - {{ __('main.product.characteristics')}}</h3>
        <p class="ml-4">{{ $product->characteristics}}</p>
        @endif

        @if($product->application !== null)
        <h3 class="lead">- {{ __('main.product.application')}}</h3>
        <p class="ml-4">{{ $product->application}}</p>
        @endif

        @if($product->components !== null)
        <h3 class="lead">- {{ __('main.product.components')}}	</h3>
        <p class="ml-4">{{ $product->components}}</p>
        @endif

        @if($product->type !== null)
        <h3 class="lead">- {{ __('main.product.type')}}	</h3>
        <p class="ml-4">{{ $product->type}}</p>
        @endif

        @if($product->edge !== null)
        <h3 class="lead">- {{ __('main.product.edge')}}	</h3>
        <p class="ml-4">{{ $product->edge}}</p>
        @endif

        @if($product->quality !== null)
        <h3 class="lead">- {{ __('main.product.quality')}}	</h3>
        <p class="ml-4">{{ $product->quality}}</p>
        @endif

        <p class="lead">- {{ __('main.product.safety_signs')}}</p>
        <p class="ml-4"> 
          @foreach($product->signs as $sign)           
            <img src="{{$sign->img_url}}" alt="{{$sign->label_en}}" data-toggle="tooltip" data-placement="top" title="@if(App::isLocale('ar')) {{$sign->label_ar}} @else {{$sign->label_en}} @endif">
          @endforeach
        </p>

      </div>
    </div>

    <hr class="my-5">

    <div class="row">
      <div class="col-md-12 mb-4">
        <table class="table table-bordered" id="productsTable">
          <thead>
            <tr>
              @if($product->quality == null)
              <th scope="col">{{ __('main.product.dim')}}</th>
              <th scope="col">{{ __('main.product.shape')}}</th>
              <th scope="col">{{ __('main.product.max_speed')}}</th>
              <th scope="col">{{ __('main.product.max_rpm')}}</th>
              @else 
              <th scope="col">{{ __('main.product.dim')}}</th>
              <th scope="col">{{ __('main.product.max_speed')}}</th>
              <th scope="col">{{ __('main.product.max_rpm')}}</th>
              <th scope="col">{{ __('main.product.segement_no')}}</th>
              <th scope="col">{{ __('main.product.segement_thickness')}}</th>
              <th scope="col">{{ __('main.product.segement_height')}}</th>
              <th scope="col">{{ __('main.product.segement_quality')}}</th>
              @endif
            </tr>
          </thead>
          <tbody>
          @foreach($product->sizes as $size)
          <tr>
            <!-- Dim -->
            @if($size->thickness_mm == null) 
            <td>{{$size->width_mm}}×{{$size->bore_mm}}</td>
            @else
            <td>{{$size->width_mm}}×{{$size->thickness_mm}}×{{$size->bore_mm}}</td>
            @endif
            
            <!-- shape -->
            @if($size->shape != null)
              <td>
              @if($size->shape == "depressed")
                <img src="{{'/assets/images/signs/shape-depressed.png'}}" alt="">
              @else 
                <img src="{{'/assets/images/signs/shape-flat.png'}}" alt="">
              @endif
              </td>
            @endif


            <td>{{$size->max_speed}} m/s</td>
            <td>{{$size->max_rpm}}</td>
            @if($size->segment_no != null)
            <td>{{$size->segment_no}}</td>
            @endif

            @if($size->segment_thickness != null)
            <td>{{$size->segment_thickness}}</td>
            @endif

            @if($size->segment_height != null)
            <td>{{$size->segment_height}}</td>
            @endif

            @if($size->quality != null)
            <td>{{$size->quality}}</td>
            @endif
          </tr>
          @endforeach
          </tbody>
        </table>
      </div>
    </div>


    <hr class="my-5">

    <div class="row">
      <div class="col-md-6 px-4">
        <p class="lead ">{{ __('main.product.product_data')}}</p>
        <ul class="list-unstyled">
          <li class="ml-2"><span><img src="/assets/images/icon-pdf.png" alt="">  </span><a href="{{$data_sheet->file_url}}">{{$data_sheet->title}}</a></li>
        </ul>
      </div>

      <div class="col-md-6 px-4">
        <p class="lead">{{ __('main.product.service_material')}}</p>
        <ul class="list-unstyled">
          @foreach($service_material as $sm)
            @if($size->quality != null) 
              @if($sm->diamond == 1)
              <li class="ml-2"><span><img src="/assets/images/icon-pdf.png" alt="">  </span><a href="{{$sm->file_url}}">{{$sm->title}}</a></li>
              @endif
            @else
              @if($sm->diamond == 0)
              <li class="ml-2"><span><img src="/assets/images/icon-pdf.png" alt="">  </span><a href="{{$sm->file_url}}">{{$sm->title}}</a></li>
              @endif
            @endif
          @endforeach


        </ul>
      </div>
    </div>
    </div>
  </div>
</main>
<!--end main-->
@endsection