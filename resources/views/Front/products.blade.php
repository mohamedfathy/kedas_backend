@extends('Front.layouts.02_master')
@section('title', 'Products')
@section('content')
<!--start section-->
<section class="main-section bg-cover skewed" style="background-image: url('/assets/images/background.jpg');">
  <div class="overlay">
    <div class="container unskewed">
      <div class="row">
        <div class="col-12">
          <h1 class="text-center nunito-heading text-capitalize">{{$category->name_en}}</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!--end section-->

<!-- start main--> 
<main class="conpag-main text-dark">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 mb-5"><h1 class="text-center text-capitalize">{{$category->name_en}}</h1></div>
      
      <div class="card-deck" style="width:100%;">
        @foreach ($products as $product)
        <div class="col-md-4 mb-4">
          <div class="card h-100 w-100">
            <img src="{{$product->img_url}}" class="card-img-top" alt="{{$product->name}}" style="background-color:#f0f0f0;">
            <div class="card-body">
              <h5 class="card-title">{{$product->name}}</h5>
              <p class="card-text">{{$product->application}}</p>
              <a href="/{{ app()->getLocale() }}/product/{{$product->id}}" class="btn btn-warning stretched-link">Explore...</a>
            </div>
          </div>
        </div>
        @endforeach
      </div>

    </div>
  </div>
</main>
<!--end main-->
@endsection