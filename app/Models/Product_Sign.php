<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product_Sign extends Model
{
    public $table = 'product_signs';
    protected $timestamps = false;
}
