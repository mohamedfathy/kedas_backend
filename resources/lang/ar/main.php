<?php
return [
    // general files
    'title' => 'كيداس',
    'home' => 'الرئيسية',
    'categories' => 'فئات المنتج',
    'products' => 'المنتجات',
    'product' => 'المنتج',
    'news' => 'الاخبار',
    'about' => 'عن الموقع',
    'contact' => 'اتصل بنا',

    // Front/layouts/03_footer.blade.php
    'footer.contact_us' => 'تواصل معنا',
    'footer.first_address' => '78 شارع عدنان المالكي - المنيا - مصر',
    'footer.second_address' => 'شارع العهد الجديد - سمالوط - المنيا - مصر',
    'footer.browse' => 'تصفح',
    'footer.start_copyright' => 'حقوق النشر',
    'footer.company_copyright' => 'شركة كيداس',
    'footer.end_copyright' => 'جميع الحقوق محفوظة',

    // Front/partials/contact.blade.php
    'contact.lets_talk' => 'دعنا نتحدث',
    'contact.name' => 'الاسم',
    'contact.name_validation' => 'برجاء اضافة الاسم',
    'contact.phone' => 'التيلفون',
    'contact.phone_validation' => 'برجاء اضافة التليفون',
    'contact.message' => 'الرسالة',
    'contact.message_validation' => 'برجاء اضافة الرسالة',
    'contact.send' => 'ارسل',

    // Front/home.blade.php
    'home.company_slogan' => 'خبراء احجار القطع فائقة الجودة',
    'home.company_title' => 'الشركة المصرية',
    'home.exclusive_partner' => 'الشريك الحصري فى مصر',
    'home.owner_name' => 'م/ احمد عبدالله',
    'home.company_owner' => 'مالك و مدير الشركة المصرية',
    'home.company_story' => 'تاسست الشركة عام 1999 و منذ ذلك الوقت تعمل الشركة فى توزيع مواد البناء والمعدات والالات',
    'home.read_more' => 'اقرا المزيد',

    // Front/about.blade.php
    'about.our_story' => 'قصتنا',
    'about.company_story' => 'تاسست الشركة عام 1999 و منذ ذلك الوقت تعمل الشركة  فى توزيع مواد البناء و المعدات و الالات . 
    يدير الشركة مالكها المهندس / احمد عبدالله محمد عبدالله.
    المصرية هي الوكيل والموزع الوحيد لكبرى شركات مواد البناء داخل وخارج جمهورية مصر العربية.
    في عام 2012 ، تم توقيع عقد بين KEDAS GROUP - المزود لقطع الأقراص والطحن وشفرات الماس - من ألمانيا والمصرية لتكون الموزع الوحيد والحصري لمنتجات KEDAS GROUP في مصر.',

    // Front/categories.blade.php
    'categories.categories' => 'فئات المنتج',
    'categories.our_categories' => 'فئاتنا',
    'categories.explore' => 'استكشف',

    // Front/news.blade.php
    'news.hardware_exhibition' => 'معرض العدد و الالات 2012',
    'news.exhibtion_description' => 'الشريك المصري و فريق شركة كداس فى المعرض الدولي للعدد و الالات بمدينة cologne عام 2012',

    // Front/product.blade.php
    'product.characteristics' => 'الخصائص',
    'product.application' => 'التطبيقات',
    'product.components' => 'المكونات',
    'product.type' => 'النوع',
    'product.edge' => 'الحافة',
    'product.quality' => 'الجودة',
    'product.safety_signs' => 'علامات الامان',
    'product.dim' => 'الابعاد',
    'product.shape' => 'الشكل',
    'product.max_speed' => 'اقصي سرعة',
    'product.max_rpm' => 'اقصى عدد لفات',
    'product.segement_no' => 'رقم القطعة',
    'product.segement_thickness' => 'ثخانة القطعة',
    'product.segement_height' => 'ارتفاع القطعة',
    'product.segement_quality' => 'الجودة',
    'product.product_data' => 'بيانات المنتج',
    'product.service_material' => 'مواد الخدمة',
];