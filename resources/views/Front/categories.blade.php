@extends('Front.layouts.02_master')
@section('title', __('main.categories'))
@section('content')
<!--start section-->
<section class="main-section bg-cover skewed" style="background-image: url('/assets/images/background.jpg');">
  <div class="overlay">
    <div class="container unskewed">
      <div class="row">
        <div class="col-12">
          <h1 class="text-center nunito-heading">{{ __('main.categories.categories')}}</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!--end section-->

<!-- start main--> 
<main class="conpag-main text-dark">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 mb-5"><h1 class="text-center">{{ __('main.categories.our_categories')}}</h1></div>
      @foreach ($categories as $item)
      <div class="col-lg-6 mb-4">
        <div class="card">
          <img src="{{$item->image_url}}" class="card-img-top" alt="{{$item->name_en}}">
          <div class="card-body">
            <h5 class="card-title text-capitalize">{{$item->name_en}}</h5>
            <p class="card-text">{{$item->description}}</p>
            <a href="/{{app()->getLocale()}}/products/{{$item->id}}" class="btn btn-warning stretched-link">{{ __('main.categories.explore')}}</a>
          </div>
        </div>
      </div>
      @endforeach        

    </div>
  </div>
</main>
<!--end main-->
@endsection