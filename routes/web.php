<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/en');
Route::group(['prefix' => '{language}'], function () {
  Route::get('/', 'Front\FrontController@home')->name('home');
  Route::post('/complaints', 'Front\FrontController@complaints')->name('complaints');
  Route::get('/categories', 'Front\FrontController@categories')->name('categories');
  Route::get('/products/{category_id}', 'Front\FrontController@products')->name('products');
  Route::get('/product/{product_id}', 'Front\FrontController@product')->name('product');
  Route::get('/news', 'Front\FrontController@news')->name('news');
  Route::get('/contact', 'Front\FrontController@contact')->name('contact');
  Route::get('/about', 'Front\FrontController@about')->name('about');
});