$(document).ready(function () {
  // ============================================  header ====================================
  function stickyHeader() {
      var header = $(".header");
      if ($(this).scrollTop() > 80) {
          header.addClass("sticky");
      } else {
          header.removeClass("sticky");
      }
  }
  stickyHeader();
  //sticky header
  $(window).on("scroll", function () {
      stickyHeader();
  });
  $(".header .navbar-toggler").addClass("collapsed");

  // scroll down btn
  $(".scroll-btn").on("click", function () {
      $("html").animate({
          scrollTop: $(".main-section").outerHeight()
      })
  });
  // ======================================= tooltip ==========================================
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });
});