@extends('Front.layouts.02_master')
@section('title', __('main.contact'))
@section('content')
<!--start section-->
<section class="main-section bg-cover skewed" style="background-image: url('/assets/images/background.jpg');">
  <div class="overlay">
    <div class="container unskewed">
      <div class="row">
        <div class="col-12">
          <h1 class="text-center nunito-heading">{{ __('main.contact') }}</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!--end section-->

<!-- start main--> 
<main class="conpag-main text-dark">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 our-partner-box text-center offset-lg-2">
        <h1 class="display-4 mb-5">{{ __('main.contact') }}</h1>
        <div class="row justify-content-between">
          <div class="col-lg-5 col-md-5 contactus-contactbox m-b-30">
            <div class="phones-box">
              <div class="contactus-contact-icon d-flex">
                <i class="fas fa-phone font-icon"></i>
              </div>
              <ul class="list-unstyled contact-links">
                @foreach($phones as $phone)  
                <li style="  direction: ltr;display: inline-block;">{{$phone->phone}} </li> 
                @endforeach
              </ul>
            </div>
          </div>
          <div class="col-lg-5 col-md-5 contactus-contactbox m-b-30">
            <div class="phones-box">
              <div class="contactus-contact-icon d-flex">
                <i class="fas fas fa-envelope font-icon"></i>
              </div>
              <ul class="list-unstyled contact-links">
              <ul class="list-unstyled contact-links">
                @foreach($emails as $email)  
                <li style="  direction: ltr;display: inline-block;">{{$email->email}} </li> 
                @endforeach
              </ul>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
<!--end main-->

@include('Front.partials.contact')
@include('Front.partials.map')
@endsection