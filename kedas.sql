-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 14, 2020 at 12:48 PM
-- Server version: 8.0.18
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kedas`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name_ar` varchar(255) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name_ar`, `name_en`, `description`, `image_url`) VALUES
(1, '', 'inox', 'Some quick example text to build on the card title and make up the bulk of the card\'s content.', '/assets/images/categories/inox.jpg'),
(2, '', 'metal', 'Some quick example text to build on the card title and make up the bulk of the card\'s content.', '/assets/images/categories/metal.jpg'),
(3, '', 'stone', 'Some quick example text to build on the card title and make up the bulk of the card\'s content.', '/assets/images/categories/stone.jpg'),
(4, '', 'diamond', 'Some quick example text to build on the card title and make up the bulk of the card\'s content.', '/assets/images/categories/diamond.jpg'),
(5, '', 'cutting', 'Some quick example text to build on the card title and make up the bulk of the card\'s content.', '/assets/images/categories/cutting.jpg'),
(6, '', 'grinding', 'Some quick example text to build on the card title and make up the bulk of the card\'s content.', '/assets/images/categories/grinding.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE `complaints` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `complaints`
--

INSERT INTO `complaints` (`id`, `name`, `phone`, `message`) VALUES
(1, 'Mohamed', '01008292985', 'Hello');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `img_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `characteristics` text,
  `application` varchar(255) DEFAULT NULL,
  `components` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `edge` varchar(100) DEFAULT NULL,
  `quality` varchar(100) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `img_url`, `characteristics`, `application`, `components`, `type`, `edge`, `quality`, `is_active`, `category_id`) VALUES
(1, 'AZ 24 P-BF INOX', NULL, 'very high stock removal, very long service life, resin bonded, very productive', 'alloy stainless steel, container structural steel, non-alloy stainless steel, stainless steel, structural steel, titanium', 'aluminum zirconium\r\n', NULL, NULL, NULL, 1, 1),
(2, 'AZ 36 P-BF INOX', NULL, 'fast cutting, long service life, resin bonded, very productive', 'sheet steel, stainless steel, thin profile steel', 'aluminum zirconium', NULL, NULL, NULL, 1, 1),
(3, 'AZ 46 P-BF INOX', NULL, 'long service life, very fast cutting, resin bonded, very productive', 'alloy stainless steel, aluminium, container structural steel, non-alloy stainless steel, stainless steel, titanium', 'aluminum zirconium\r\n', NULL, NULL, NULL, 1, 1),
(4, 'AZ 60 P-BF INOX', NULL, 'extremely fast cutting, very long service life, resin bonded, very productive', 'alloy stainless steel, aluminium, container structural steel, non-alloy stainless steel, stainless steel, titanium', 'aluminum zirconium', NULL, NULL, NULL, 1, 1),
(5, 'A 24 PLUS T-BF', NULL, 'extremely fast cutting, very long service life, resin bonded, very productive', 'hard steel, rail, sheet steel, steel, structural steel, tool steel', 'aluminium oxide', NULL, NULL, NULL, 1, 2),
(6, 'A 30 PLUS T-BF', NULL, 'very high stock removal, very long service life, resin bonded, very productive', 'cast steel, steel, structural steel, tool steel', 'aluminium oxide', NULL, NULL, NULL, 1, 2),
(7, 'C 30 PLUS T-BF', NULL, 'very high stock removal, very long service life, resin bonded, very productive', 'building materials, concrete, marble, stone', 'silicon carbide', NULL, NULL, NULL, 1, 3),
(8, 'CR 19', NULL, NULL, 'High-performance diamond blades for tiles, ceramic, slate, limestone, and stone (wet cutting for ceramic)\r\n\r\n', NULL, 'Sintered', 'Closed', 'available in Professional (P) or Standard (S)', 1, 4),
(9, 'SS 21', NULL, NULL, 'High-performance diamond blades for concrete, brick, ceramic, slate, stone, reinforced concrete, concrete paving, and concrete roofing tiles', NULL, 'Sintered', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 4),
(10, 'TW 22', NULL, NULL, 'High-performance diamond blades for concrete, brick, ceramic and slate', NULL, 'Sintered', 'Closed', 'available in Professional (P) or Standard (S)', 1, 4),
(11, 'T 56', NULL, NULL, 'High-performance diamond blades for concrete, brick, ceramic, slate and reinforced concrete', NULL, 'Sintered', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 4),
(12, 'ST 65', NULL, NULL, 'High-performance diamond blades for concrete, brick, slate, reinforced concrete, concrete paving, concrete roofing tiles, fire-proof stone, marble, and granite', NULL, 'Sintered', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 4),
(13, 'SP 12', NULL, NULL, 'High-performance diamond blades for concrete, brick, slate, sand-lime stone, sandstone, marble, granite, fire-proof stone, and fireclay', NULL, 'Sintered', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 4),
(14, 'LC 69', NULL, NULL, 'High-performance diamond blades for stone, concrete, cement pipes, cement slabs', NULL, 'Laser', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 4),
(15, 'LA 96', NULL, NULL, 'High-performance diamond blades for asphalt, concrete slabs, concrete paving', NULL, 'Laser', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 4),
(16, 'LM 15', NULL, NULL, 'High-performance diamond blades for stone and marble', NULL, 'Laser', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 4),
(17, 'LG 91', NULL, NULL, 'High-performance diamond blades for hard stone and granite', NULL, 'Laser', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 4),
(18, 'BC 51', NULL, NULL, 'High-performance diamond blades for hard stone and granite', NULL, 'Silver Brazed', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 4),
(19, 'BU 20', NULL, NULL, 'Universal diamond blade for building material', NULL, 'Silver Brazed', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 4),
(20, 'BM 62', NULL, NULL, 'High-performance diamond blades for stone and marble', NULL, 'Silver Brazed', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 4),
(21, 'BG 92', NULL, NULL, 'High-performance diamond blades for stone, marble and granite', NULL, 'Silver Brazed', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 4),
(22, 'AZ 36 P-BF INOX', NULL, 'fast cutting, long service life, resin bonded, very productive', 'sheet steel, stainless steel, thin profile steel', 'aluminum zirconium', NULL, NULL, NULL, 1, 5),
(23, 'AZ 46 P-BF INOX', NULL, 'long service life, very fast cutting, resin bonded, very productive', 'alloy stainless steel, aluminium, container structural steel, non-alloy stainless steel, stainless steel, titanium', 'aluminum zirconium\r\n', NULL, NULL, NULL, 1, 5),
(24, 'AZ 60 P-BF INOX', NULL, 'extremely fast cutting, very long service life, resin bonded, very productive', 'alloy stainless steel, aluminium, container structural steel, non-alloy stainless steel, stainless steel, titanium', 'aluminum zirconium', NULL, NULL, NULL, 1, 5),
(25, 'A 24 PLUS T-BF', NULL, 'extremely fast cutting, very long service life, resin bonded, very productive', 'hard steel, rail, sheet steel, steel, structural steel, tool steel', 'aluminium oxide', NULL, NULL, NULL, 1, 5),
(26, 'C 30 PLUS T-BF', NULL, 'very high stock removal, very long service life, resin bonded, very productive', 'building materials, concrete, marble, stone', 'silicon carbide', NULL, NULL, NULL, 1, 5),
(27, 'CR 19', NULL, NULL, 'High-performance diamond blades for tiles, ceramic, slate, limestone, and stone (wet cutting for ceramic)\r\n\r\n', NULL, 'Sintered', 'Closed', 'available in Professional (P) or Standard (S)', 1, 5),
(28, 'SS 21', NULL, NULL, 'High-performance diamond blades for concrete, brick, ceramic, slate, stone, reinforced concrete, concrete paving, and concrete roofing tiles', NULL, 'Sintered', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 5),
(29, 'TW 22', NULL, NULL, 'High-performance diamond blades for concrete, brick, ceramic and slate', NULL, 'Sintered', 'Closed', 'available in Professional (P) or Standard (S)', 1, 5),
(30, 'T 56', NULL, NULL, 'High-performance diamond blades for concrete, brick, ceramic, slate and reinforced concrete', NULL, 'Sintered', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 5),
(31, 'ST 65', NULL, NULL, 'High-performance diamond blades for concrete, brick, slate, reinforced concrete, concrete paving, concrete roofing tiles, fire-proof stone, marble, and granite', NULL, 'Sintered', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 5),
(32, 'SP 12', NULL, NULL, 'High-performance diamond blades for concrete, brick, slate, sand-lime stone, sandstone, marble, granite, fire-proof stone, and fireclay', NULL, 'Sintered', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 5),
(33, 'LC 69', NULL, NULL, 'High-performance diamond blades for stone, concrete, cement pipes, cement slabs', NULL, 'Laser', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 5),
(34, 'LA 96', NULL, NULL, 'High-performance diamond blades for asphalt, concrete slabs, concrete paving', NULL, 'Laser', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 5),
(35, 'LM 15', NULL, NULL, 'High-performance diamond blades for stone and marble', NULL, 'Laser', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 5),
(36, 'LG 91', NULL, NULL, 'High-performance diamond blades for hard stone and granite', NULL, 'Laser', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 5),
(37, 'BC 51', NULL, NULL, 'High-performance diamond blades for hard stone and granite', NULL, 'Silver Brazed', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 5),
(38, 'BU 20', NULL, NULL, 'Universal diamond blade for building material', NULL, 'Silver Brazed', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 5),
(39, 'BM 62', NULL, NULL, 'High-performance diamond blades for stone and marble', NULL, 'Silver Brazed', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 5),
(40, 'BG 92', NULL, NULL, 'High-performance diamond blades for stone, marble and granite', NULL, 'Silver Brazed', 'Segmented', 'available in Professional (P) or Standard (S)', 1, 5),
(41, 'AZ 24 P-BF INOX', NULL, 'very high stock removal, very long service life, resin bonded, very productive', 'alloy stainless steel, container structural steel, non-alloy stainless steel, stainless steel, structural steel, titanium', 'aluminum zirconium\r\n', NULL, NULL, NULL, 1, 6),
(42, 'A 30 PLUS T-BF', NULL, 'very high stock removal, very long service life, resin bonded, very productive', 'cast steel, steel, structural steel, tool steel', 'aluminium oxide', NULL, NULL, NULL, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `product_data`
--

CREATE TABLE `product_data` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `file_url` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_sizes`
--

CREATE TABLE `product_sizes` (
  `id` int(11) NOT NULL,
  `width_mm` smallint(6) NOT NULL,
  `thickness_mm` smallint(6) DEFAULT NULL,
  `bore_mm` smallint(6) NOT NULL,
  `width_inch` smallint(6) NOT NULL,
  `thickness_inch` smallint(6) DEFAULT NULL,
  `bore_inch` smallint(6) NOT NULL,
  `shape` enum('flat','depressed') DEFAULT NULL,
  `max_speed` smallint(6) NOT NULL,
  `max_rpm` smallint(6) NOT NULL,
  `segement_no` smallint(6) DEFAULT NULL,
  `segement_thickness` smallint(6) DEFAULT NULL,
  `segement_height` smallint(6) DEFAULT NULL,
  `quality` enum('standard','professional') DEFAULT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `service_materials`
--

CREATE TABLE `service_materials` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `file_url` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `product_data`
--
ALTER TABLE `product_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_sizes`
--
ALTER TABLE `product_sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_materials`
--
ALTER TABLE `service_materials`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `product_data`
--
ALTER TABLE `product_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_sizes`
--
ALTER TABLE `product_sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `service_materials`
--
ALTER TABLE `service_materials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
