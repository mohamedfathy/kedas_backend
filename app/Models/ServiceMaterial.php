<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceMaterial extends Model
{
    protected $table = "service_materials";
    public $timestamps = false;
}
